class Hangman
  TOTAL_CHANCES = 6
  attr_reader :guesser, :referee, :board

  def initialize(players) # passing  a hash as method argument
    @guesser = players[:guesser]
    @referee = players[:referee]
    @remain_chances = TOTAL_CHANCES
  end

  def play
    setup
    until @remain_chances == 0 || won?
      puts @board
      take_turn
    end
    puts game_over
  end

  def game_over
    if @guesser.won? || @referee.won?
      "#{@guesser} won!"
    else
      "#{@guesser} lost. The word #{@guesser.display_word}."
    end
  end

  def won?
    true
  end

  def setup
    secret_word = @referee.pick_secret_word
    @guesser.register_secret_length(secret_word)
    @board = [nil]*secret_word
  end

  def take_turn
    guess = @guesser.guess(@board)
    indices = @referee.check_guess(guess)
    update_board(guess, indices)
    @remain_chances -= 1 if indices.empty?
    @guesser.handle_response(guess, indices)
  end

  def update_board(guess, indices)
    indices.each { |idx| @board[idx] = guess}
  end

end

class HumanPlayer
  def pick_secret_word
    print "choose a secret word and tell me the length"
    begin
      Integer(gets.chomp)
    rescue ArgumentError
      puts "Enter a valid length"
    end
  end

  def register_secret_length
    puts "Secret word length is #{length}"
  end

  def guess(board)
    p board
    puts "Guess a letter:"
    gets.chomp
  end

  def check_guess(guess)
    puts "Player guess #{guess}"
    puts "What position hit"
    gets.chomp.split(",").map(&:to_i)
  end

  def handle_response(guess, anwser)
    puts "Great I found #{guess} at position #{answer} "
  end

  def display_word
    puts " What secret word did you chose?"
    gets.chomp
  end

end

class ComputerPlayer
  attr_reader :candidate_words

  def read_file
    File.new("dictionary.txt").readlines.each do |line|
      @dictionary << line.chomp
    end
  end

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(guess)
    anwser = []
    @secret_word.split("").each_with_index do |letter, index|
      anwser << index if letter == guess
    end

    anwser
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select { |word| word.length == length }
  end

  def guess(board)
    frequency = frequency(board)
    most_frequent_letters = frequency.sort_by { |letter, count| count }
    letter, _ = most_frequent_letters.last
    letter
  end

  def handle_response(guess, response_indices)
    @candidate_words.reject! do |word|
      delete_word = false
      word.split("").each_with_index do |letter, index|
        if letter == guess && !response_indices.include?(index)
          delete_word = true
          break
        elsif letter != guess && response_indices.include?(index)
          delete_word = true
          break
        end
      end

      delete_word
    end
  end

  private
  def frequency(board)
    frequency = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |letter, index|
        frequency[word[index]] += 1 if letter.nil?
      end
    end

    frequency
  end
end
